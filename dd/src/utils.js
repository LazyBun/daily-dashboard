import axios from 'axios'

// export function url() {
//     let res = endpoint;
//     for (let i = 0; i < arguments.length; ++i) {
//         let segment = arguments[i];
//         if (segment.startsWith('/')) {
//             segment = segment.substr(1);
//         }

//         if (startsWith(segment, '?') || res[res.length - 1] === '/') {
//             res += segment;
//         } else {
//             res += `/${segment}`;
//         }
//     }
//     return res;
// }

export async function proxyGet(host, path) {
    return await axios.post('/proxy', {
        hostname: host,
        path: path,
        method: 'GET'
    })
}

export async function proxyPost(host, path) {
    return await axios.post('/proxy', {
        hostname: host,
        path: path,
        method: 'POST'
    })
}
