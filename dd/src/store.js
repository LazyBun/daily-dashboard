import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'

let json = require('../private-data.json')

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        time: {},
        currentLang: 'en',
        protectedData: json
    },
    mutations: {
        incrementTime (state) {
            moment.locale(state.currentLang)
            state.time = moment()
        },
        setLang (state, lang) {
            state.currentLang = lang
        }
    }
})
