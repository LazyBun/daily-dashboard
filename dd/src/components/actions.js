import axios from 'axios'
let voice = window.responsiveVoice

export function slackIncoming(data) {
    if (data && data.slack && data.slack.msg && data.slack.url) {
        // TODO: msg sucks atm, use some lib to properly escape
        axios.post(data.slack.url, 'payload={"text":"' + data.slack.msg + '"}').then(
            () => { console.log('Slack incoming hook succeeded') },
            () => { console.log('Slack incoming hook failed :(') }
        )
    } else {
        badData('slack incoming message')
    }
}

export function speak(data) {
    // TODO: Other params, pitch, speed etc
    if (data && data.speak && data.speak.msg) {
        voice.speak(data.speak.msg)
    } else {
        badData('speaking message')
    }
}

function badData(what) {
    console.log('Data for ' + what + ' is undefined.')
}
