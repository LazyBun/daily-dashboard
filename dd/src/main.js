// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Resource from 'vue-resource'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false

Vue.use(Resource)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

axios.interceptors.response.use(
    (response) => {
        return response
    },
    (error) => {
        window.$.notify({ message: error }, { type: 'danger' })
        return Promise.reject(error)
    }
)
